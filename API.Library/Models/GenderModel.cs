﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Library.Models
{
    public class GenderModel
    {
        public int Id { get; set; }
        public string GenderName { get; set; }
    }
}
