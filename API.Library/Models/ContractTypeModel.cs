﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Library.Models
{
    public class ContractTypeModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
