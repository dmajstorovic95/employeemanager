﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Library.Models
{
    public class DepartmentModel
    {
        public int Id { get; set; }
        public string DepartmentName { get; set; }
    }
}
