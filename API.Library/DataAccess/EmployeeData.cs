﻿using API.Library.Internal.DataAccess;
using API.Library.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Library.DataAccess
{
    public class EmployeeData : IEmployeeData
    {
        private readonly ISqlDataAccess _sql;

        public EmployeeData(ISqlDataAccess sql)
        {
            _sql = sql;
        }

        public List<EmployeeModel> GetEmployeeById(string id)
        {
            throw new NotImplementedException();
        }

        public List<EmployeeModel> GetEmployees()
        {
            var output = _sql.LoadData<EmployeeModel, dynamic>("dbo.spEmployee_GetAll", new { }, "EmployeesData");
            return output;
        }

        public void AddEmployee(EmployeeModel employee)
        {
            _sql.SaveData("dbo.spEmployee_AddNew", employee, "EmployeesData");
        }
    }
}
