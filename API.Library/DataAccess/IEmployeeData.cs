﻿using API.Library.Models;
using System.Collections.Generic;

namespace API.Library.DataAccess
{
    public interface IEmployeeData
    {
        List<EmployeeModel> GetEmployeeById(string id);
        List<EmployeeModel> GetEmployees();
        void AddEmployee(EmployeeModel employee);
    }
}