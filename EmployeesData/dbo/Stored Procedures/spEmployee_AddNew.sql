﻿CREATE PROCEDURE [dbo].[spEmployee_AddNew]
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@PhotoPath nvarchar(MAX),
	@Gender nvarchar(50),
	@BirthYear smallint,
	@StartDate date,
	@ContractType nvarchar(50),
	@ContractEndDate date,
	@Department nvarchar(50),
	@VacationDays tinyint,
	@FreeDays tinyint,
	@PaidLeaveDays tinyint
AS
begin
	set nocount on;

	insert into dbo.Employee(FirstName, LastName, PhotoPath, Gender, BirthYear, StartDate, ContractType, ContractEndDate, Department, VacationDays, FreeDays, PaidLeaveDays)
	values (@FirstName, @LastName, @PhotoPath, @Gender, @BirthYear, @StartDate, @ContractType, @ContractEndDate, @Department, @VacationDays, @FreeDays, @PaidLeaveDays);
end
