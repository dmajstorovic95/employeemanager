﻿CREATE PROCEDURE [dbo].[spEmployee_GetAll]
AS
begin
	set nocount on;

	SELECT Id, FirstName, LastName, PhotoPath, Gender, BirthYear, StartDate, ContractType, 
		ContractEndDate, Department, VacationDays, FreeDays, PaidLeaveDays from dbo.Employee;
end
