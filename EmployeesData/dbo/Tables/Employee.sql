﻿CREATE TABLE [dbo].[Employee]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FirstName] NVARCHAR(50) NOT NULL, 
    [LastName] NVARCHAR(50) NOT NULL, 
    [PhotoPath] NVARCHAR(MAX) NULL, 
    [Gender] NVARCHAR(50) NOT NULL, 
    [BirthYear] SMALLINT NOT NULL, 
    [StartDate] DATE NOT NULL, 
    [ContractType] NVARCHAR(50) NOT NULL, 
    [ContractEndDate] DATE NULL, 
    [Department] NVARCHAR(50) NOT NULL, 
    [VacationDays] TINYINT NULL, 
    [FreeDays] TINYINT NULL, 
    [PaidLeaveDays] TINYINT NULL
)
