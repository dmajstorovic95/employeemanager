﻿CREATE TABLE [dbo].[ContractType]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Type] NVARCHAR(50) NOT NULL
)
