﻿CREATE TABLE [dbo].[Gender]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [GenderName] NCHAR(10) NOT NULL
)
