﻿CREATE TABLE [dbo].[Department]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [DepartmentName] NVARCHAR(50) NOT NULL
)
