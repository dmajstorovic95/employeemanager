﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeUI.Core.Models
{
    public class EmployeeModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhotoPath { get; set; }
        public string Gender { get; set; }
        public int BirthYear { get; set; }
        public DateTime StartDate { get; set; }
        public string ContractType { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public string Department { get; set; }
        public int? VacationDays { get; set; }
        public int? FreeDays { get; set; }
        public int? PaidLeaveDays { get; set; }
    }
}
