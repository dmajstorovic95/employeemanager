﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace EmployeeUI.Core.Api
{
    public class ApiHelper : IApiHelper
    {
        public ApiHelper()
        {
            InitializeClient();
        }

        private HttpClient _apiClient;

        public HttpClient ApiClient
        {
            get { return _apiClient; }
        }

        private void InitializeClient()
        {
            //string api = ConfigurationManager.AppSettings["api"];
            string api = "https://localhost:5001/";

            _apiClient = new HttpClient();
            _apiClient.BaseAddress = new Uri(api);
            _apiClient.DefaultRequestHeaders.Accept.Clear();
            _apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

    }
}
