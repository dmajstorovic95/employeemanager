﻿using System.Net.Http;

namespace EmployeeUI.Core.Api
{
    public interface IApiHelper
    {
        HttpClient ApiClient { get; }
    }
}