﻿using EmployeeUI.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeeUI.Core.Api
{
    public interface IEmployeeEndpoint
    {
        Task<List<EmployeeModel>> GetAll();
        Task Post(EmployeeModel employee);
    }
}