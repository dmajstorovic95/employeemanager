﻿using EmployeesUI.Wpf.Pages;
using EmployeeUI.Core.Api;
using EmployeeUI.Core.Models;
using Stylet;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EmployeesUI.Wpf.ViewModels
{
    public class AddNewViewModel : Screen
    {

        private readonly IEmployeeEndpoint _employeeEndpoint;

        public AddNewViewModel(IEmployeeEndpoint employeeEndpoint)
        {
            _employeeEndpoint = employeeEndpoint;
        }

        private string _photoPath = "";

        public string PhotoPath
        {
            get { return _photoPath; }
            set { _photoPath = value; }
        }


        public BindableCollection<string> Genders
        {
            get
            {
                return new BindableCollection<string>(new string[] { "Male", "Female", "Other" });
            }
        }

        private string _selectedGender;

        public string SelectedGender
        {
            get { return _selectedGender; }
            set 
            {
                SetAndNotify(ref _selectedGender, value);
                NotifyOfPropertyChange(() => CanAddNewEmployee);
            }
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set 
            { 
                SetAndNotify(ref _firstName, value);
                NotifyOfPropertyChange(() => CanAddNewEmployee);
            }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set 
            { 
                SetAndNotify(ref _lastName, value);
                NotifyOfPropertyChange(() => CanAddNewEmployee);
            }
        }

        private int _birthYear;

        public int BirthYear
        {
            get { return _birthYear; }
            set 
            { 
                SetAndNotify(ref _birthYear, value);
                NotifyOfPropertyChange(() => CanAddNewEmployee);
            }
        }

        private DateTime _startDate = DateTime.Today;

        public DateTime StartDate
        {
            get { return _startDate; }
            set 
            { 
                SetAndNotify(ref _startDate, value);
                NotifyOfPropertyChange(() => CanAddNewEmployee);
            }
        }

        private string _selectedType;

        public string SelectedType
        {
            get { return _selectedType; }
            set 
            { 
                SetAndNotify(ref _selectedType, value);
                NotifyOfPropertyChange(() => CanAddNewEmployee);
                NotifyOfPropertyChange(() => IsEndDateBoxVisible);
            }
        }

        public BindableCollection<string> Types
        {
            get
            {
                return new BindableCollection<string>(new string[] { "Definite", "Indefinite" });
            }
        }


        private DateTime _contractEndDate = DateTime.MaxValue;

        public DateTime ContractEndDate
        {
            get { return _contractEndDate; }
            set 
            { 
                SetAndNotify(ref _contractEndDate, value);
                NotifyOfPropertyChange(() => CanAddNewEmployee);
            }
        }

        public BindableCollection<string> Departments
        {
            get
            {
                return new BindableCollection<string>(new string[] { "Management", "Development", "Marketing" });
            }
        }

        private string _selectedDepartment;

        public string SelectedDepartment
        {
            get { return _selectedDepartment; }
            set 
            {
                SetAndNotify(ref _selectedDepartment, value);
                NotifyOfPropertyChange(() => CanAddNewEmployee);
            }
        }

        private int _vacationDays;

        public int VacationDays
        {
            get { return _vacationDays; }
            set { SetAndNotify(ref _vacationDays, value); }
        }

        private int _freeDays;

        public int FreeDays
        {
            get { return _freeDays; }
            set { SetAndNotify(ref _freeDays, value); }
        }

        private int _paidLeaveDays;

        public int PaidLeaveDays
        {
            get { return _paidLeaveDays; }
            set { SetAndNotify(ref _paidLeaveDays, value); }
        }

        public bool CanAddNewEmployee
        {
            get
            {
                bool output = false;

                if (FirstName?.Length > 0 && LastName?.Length > 0 && SelectedGender != null && BirthYear != 0 && IsBirthYearValid(BirthYear) && StartDate != null && SelectedType != null && SelectedDepartment != null)
                {
                    output = true;
                }

                return output;
            }
        }

        public async Task AddNewEmployee()
        {
            EmployeeModel employee = new EmployeeModel
            {
                FirstName = FirstName,
                LastName = LastName,
                BirthYear = BirthYear,
                Gender = SelectedGender,
                StartDate = StartDate,
                ContractType = SelectedType,
                ContractEndDate = ContractEndDate,
                PhotoPath = PhotoPath,
                Department = SelectedDepartment,
                VacationDays = VacationDays,
                FreeDays = FreeDays,
                PaidLeaveDays = PaidLeaveDays
            };

            await _employeeEndpoint.Post(employee);

            ((ShellViewModel)Parent).CloseItem(this);
        }

        public bool IsBirthYearValid(int year)
        {
            if (year < DateTime.UtcNow.Year - 75 || year > DateTime.UtcNow.Year - 16)
            {
                return false;
            }

            return true;    
        }

        public bool IsEndDateBoxVisible
        {
            get
            {
                bool output = false;

                if (SelectedType == "Definite")
                {
                    output = true;
                }

                return output;
            }
        }

        public void GoBack()
        {
            ((ShellViewModel)this.Parent).CloseItem(this);
        }
    }
}
