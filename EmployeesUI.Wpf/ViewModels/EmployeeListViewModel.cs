﻿using EmployeesUI.Wpf.Pages;
using EmployeeUI.Core.Api;
using EmployeeUI.Core.Models;
using Stylet;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EmployeesUI.Wpf.ViewModels
{
    public class EmployeeListViewModel : Screen
    {

        private readonly IEmployeeEndpoint _employeeEndpoint;
        private readonly AddNewViewModel _addNewVM;

        public EmployeeListViewModel(IEmployeeEndpoint employeeEndpoint, AddNewViewModel addNewVM)
        {
            _employeeEndpoint = employeeEndpoint;
            _addNewVM = addNewVM;
        }

        protected override async void OnViewLoaded()
        {
            base.OnViewLoaded();

            try
            {
                await LoadEmployees();
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected override async void OnActivate()
        {
            base.OnActivate();

            try
            {
                await LoadEmployees();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private async Task LoadEmployees()
        {
            var employeesList = await _employeeEndpoint.GetAll();
            Console.WriteLine(Environment.CurrentDirectory);
            Employees = new BindableCollection<EmployeeModel>(employeesList);
        }

        private BindableCollection<EmployeeModel> _employees;

        public BindableCollection<EmployeeModel> Employees
        {
            get { return _employees; }
            set
            {
                SetAndNotify(ref _employees, value);
            }
        }

        private EmployeeModel _selectedEmployee;

        public EmployeeModel SelectedEmployee
        {
            get { return _selectedEmployee; }
            set 
            {
                SetAndNotify(ref _selectedEmployee, value);
                NotifyOfPropertyChange(() => CanShowEmployeeDetails);
            }
        }

        public bool CanShowEmployeeDetails => SelectedEmployee != null;

        public void ShowEmployeeDetails()
        {
            ((ShellViewModel)this.Parent).ActivateItem(new EmployeeDetailsViewModel(SelectedEmployee));
        }

        public void AddNewEmployee()
        {
            ((ShellViewModel)this.Parent).ActivateItem(_addNewVM);
        }

    }
}
