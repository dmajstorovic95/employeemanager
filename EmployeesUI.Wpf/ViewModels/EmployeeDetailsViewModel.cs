﻿using EmployeesUI.Wpf.Pages;
using EmployeeUI.Core.Models;
using Stylet;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeesUI.Wpf.ViewModels
{
    public class EmployeeDetailsViewModel : Screen
    {
        public EmployeeDetailsViewModel(EmployeeModel employee)
        {
            SelectedEmployee = employee;
        }

        private EmployeeModel _selectedEmployee;

        public EmployeeModel SelectedEmployee
        {
            get { return _selectedEmployee; }
            set { _selectedEmployee = value; }
        }

        public void GoBack()
        {
            ((ShellViewModel)this.Parent).CloseItem(this);
        }

    }
}
