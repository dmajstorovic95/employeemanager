﻿using EmployeesUI.Wpf.Pages;
using EmployeesUI.Wpf.ViewModels;
using EmployeeUI.Core.Api;
using Stylet;
using StyletIoC;

namespace EmployeesUI.Wpf
{
    public class Bootstrapper : Bootstrapper<ShellViewModel>
    {
        protected override void ConfigureIoC(IStyletIoCBuilder builder)
        {
            // Configure the IoC container in here
            builder.Bind<EmployeeListViewModel>().ToSelf();
            builder.Bind<AddNewViewModel>().ToSelf();
            builder.Bind<IApiHelper>().To<ApiHelper>().InSingletonScope();
            builder.Bind<IEmployeeEndpoint>().To<EmployeeEndpoint>();
        }

        protected override void Configure()
        {
            // Perform any other configuration before the application starts

        }
    }
}
