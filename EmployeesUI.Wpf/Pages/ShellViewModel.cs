﻿using EmployeesUI.Wpf.ViewModels;
using Stylet;

namespace EmployeesUI.Wpf.Pages
{
    public class ShellViewModel : Conductor<Screen>.StackNavigation
    {
        public ShellViewModel(EmployeeListViewModel vm)
        {
            ActivateItem(vm);
        }
    }
}
