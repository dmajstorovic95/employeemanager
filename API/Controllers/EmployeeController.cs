﻿using API.Library.DataAccess;
using API.Library.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeData _employeeData;

        public EmployeeController(IEmployeeData employeeData)
        {
            _employeeData = employeeData;
        }

        [HttpGet]
        [Route("GetAllEmployees")]
        public List<EmployeeModel> Get()
        {
            return _employeeData.GetEmployees();
        }

        [HttpPost]
        [Route("AddNewEmployee")]
        public void Post(EmployeeModel employee)
        {
            _employeeData.AddEmployee(employee);
        }
    }
}
